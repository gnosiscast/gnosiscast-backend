from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from gcs.model.gnos import Gnos as GnosModel
from gcs.model.cast import CastDetail as CastDetailModel
from gcs.model.cast import Cast as CastModel


class Cast(SQLAlchemyObjectType):
    class Meta:
        model = CastModel
        interfaces = (relay.Node, )
        exclude_fields = ("master_key")


class CastDetail(SQLAlchemyObjectType):
    class Meta:
        model = CastDetailModel
        interfaces = (relay.Node, )


class Gnos(SQLAlchemyObjectType):
    class Meta:
        model = GnosModel
        interfaces = (relay.Node, )

