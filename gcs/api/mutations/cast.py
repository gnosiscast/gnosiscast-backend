from graphene import Mutation
import graphene as gph

from gcs.api.types import CastModel, Cast


class CreateCast(Mutation):
    class Arguments:
        short_name = gph.String()

    cast = gph.Field(lambda: Cast)

    def mutate(self, info, short_name: gph.String) -> Mutation:
        return Cast.produce(short_name)
