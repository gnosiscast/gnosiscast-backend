from gcs.model.mixin import Trackable, Switchable
from gcs.model.cast.cast_detail import CastDetail
from gcs.model.key import Key
from gcs.model import session

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

import re

from gcs.model import Base


class Cast(Switchable, Trackable, Base):
    """ Cast Model (article channel)"""

    __tablename__ = "casts"

    _short_name = Column("short_name", String(32), nullable=False)

    cast_detail: CastDetail = relationship("CastDetail")

    master_key = relationship("Key", primaryjoin="and_(Cast.id==Key.cast_id, Key.master==True)", uselist=False)

    @hybrid_property
    def short_name(self) -> str:
        return self._short_name

    @short_name.setter
    def short_name(self, value: str) -> None:
        self._short_name = re.sub(r"\W", "", value).lower()


    def _produce_cast_detail(self):
        CastDetail(cast_id=self.id).publish()

    def _produce_master_key(self):
        Key.produce(cast_id=self.id, read=True, write=True, master=True)

    @classmethod
    def produce(cls, short_name) -> object:
        cast = cls(short_name=short_name)

        cast.publish()

        cast._produce_cast_detail()
        cast._produce_master_key()

        return cast
