from sqlalchemy import Column, Integer, ForeignKey, String

from gcs.model import Base


class CastDetail(Base):
    """ Cast detail information """

    __tablename__ = "casts_details"

    cast_id = Column(Integer, ForeignKey("casts.id"), nullable=False)

    description = Column(String(255), server_default="''")
    title = Column(String(32), server_default="''")


