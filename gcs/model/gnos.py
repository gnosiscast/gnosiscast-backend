from gcs.model.mixin import Trackable, Switchable
from sqlalchemy import Column, String, Text, ForeignKey, Integer

from gcs.model import Base


class Gnos(Switchable, Trackable, Base):
    """Gnos - is article in Cast"""

    __tablename__ = "gnoses"

    title = Column(String(64), nullable=False)
    text = Column(Text, nullable=False)
    cast_id = Column(Integer, ForeignKey("casts.id"))

