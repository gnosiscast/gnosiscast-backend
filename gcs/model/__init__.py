""" DATABASE CONNECTION """

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from gcs.config import *

engine = create_engine(f'postgresql+psycopg2://{USER}:{PASSWORD}@{SERVER_URL}/{DBNAME}')

Session = sessionmaker(bind=engine)

session = Session()

from gcs.model.mixin import Base as BaseModel

Base = declarative_base(cls=BaseModel)

from gcs.model.key import Key
from gcs.model.cast import CastDetail
from gcs.model.cast import Cast

from gcs.model.gnos import Gnos