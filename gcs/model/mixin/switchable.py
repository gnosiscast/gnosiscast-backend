from sqlalchemy import Column, Boolean


class Switchable(object):
    """ Mixin for switch(off/on) entity """

    is_active = Column(Boolean, default=True)

