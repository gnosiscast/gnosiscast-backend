from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declared_attr

from gcs.model import session


class Base(object):
    """ Base mixin entity """

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True, autoincrement=True)


    def publish(self) -> None:
        try:
            session.add(self)
            session.commit()

        except:
            session.rollback()
