from sqlalchemy import Column, func
from sqlalchemy.dialects.postgresql import TIMESTAMP


class Trackable(object):
    """ Mixin for trackable on create and update entity """

    created_at = Column(TIMESTAMP, server_default=func.now())
    updated_at = Column(TIMESTAMP, onupdate=func.now())
