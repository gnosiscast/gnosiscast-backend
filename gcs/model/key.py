from gcs.model.mixin import Trackable, Switchable

from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.dialects.postgresql import BYTEA

from gcs.model import Base

from os import urandom
import secrets
import hashlib
import binascii

class Key(Switchable, Trackable, Base):
    """ Key use for write/read gnoses in casts"""

    __tablename__ = "keys"

    key = Column(String(64), nullable=False)
    hash_salt = Column(BYTEA, nullable=False)
    hash_rounds = Column(Integer, nullable=False)

    cast_id = Column(Integer, ForeignKey("casts.id"))

    read = Column(Boolean)
    write = Column(Boolean)
    master = Column(Boolean)

    def _generate_key(self) -> None:
        self.hash_salt = urandom(16)
        self.hash_rounds = secrets.randbelow(99) + 1

        base_key: bytes = urandom(8)

        bytes_key: bytes = hashlib.pbkdf2_hmac("sha256", base_key, self.hash_salt, self.hash_rounds)

        self.key = binascii.hexlify(bytes_key).decode()



    @classmethod
    def produce(cls, read: bool, write: bool, master: bool, cast_id: int) -> object:
        key = cls(read=read, write=write, master=master, cast_id=cast_id)

        key._generate_key()

        key.publish()

        return key
